var footageMap = {}
function FindInComp(comp, incomeN, targetN, layerInstead) {
	var n = incomeN;
	for(var i=0; i<comp.layers.length; i++){
		n = n + 1;
		var nextLayer = comp.layer(comp.layers.length - i);
		if(n == targetN) {
			if (layerInstead) return nextLayer;
			return nextLayer.source;
			}
		if(nextLayer.source.typeName == "Composition") {
			var result = FindInComp(nextLayer.source, n, targetN, layerInstead);
			if (typeof(result) == 'number') n = result;
			else return result;
		}
	}
	return n + 1;
}
function FindOrImport(path, n) {
	var foundItem = footageMap[path];
	 if(foundItem) {
		if(n) foundItem = FindInComp(foundItem, 0, n);
		return foundItem;
	 }
	var io = new ImportOptions(File(path));
	if(n) io.importAs = ImportAsType.COMP_CROPPED_LAYERS;
	var newFootage = app.project.importFile(io);
	footageMap[path] = newFootage;
	if(n) newFootage = FindInComp(newFootage, 0, n);
	return newFootage;
}
function GetLayer(path, n) {
	var comp = footageMap[path];
	var layer = FindInComp(comp, 0, n, true);
	return layer;
}

